<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'timerrelease', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   mod_timerrelease
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['bynameondate'] = 'by {$a->name} - {$a->date}';
$string['contentheader'] = 'Content';
$string['contentheader'] = 'Content';

$string['pluginname'] = 'timerrelease';
$string['countdown_title'] = 'Countdown title';

$string['dnduploadmaketimerrelease'] = 'Unzip files and create timerrelease';
$string['downloadtimerrelease'] = 'Download timerrelease';
$string['eventallfilesdownloaded'] = 'Zip archive of timerrelease downloaded';
$string['eventtimerreleaseupdated'] = 'Timer release updated';
$string['examstarts'] = 'Exam Starts';
$string['clickstart'] = 'Click to Start';
$string['timerrelease:addinstance'] = 'Add a new timerrelease';
$string['timerrelease:managefiles'] = 'Manage files in timerrelease module';
$string['timerrelease:view'] = 'View timerrelease content';
$string['timerreleasecontent'] = 'Files and subtimerreleases';
$string['forcedownload'] = 'Force download of files';
$string['forcedownload_help'] = 'Whether certain files, such as images or HTML files, should be displayed in the browser rather than being downloaded. Note that for security reasons, the setting should only be unticked if all users with the capability to manage files in the timerrelease are trusted users.';
$string['indicator:cognitivedepth'] = 'Timer release cognitive';
$string['indicator:cognitivedepth_help'] = 'This indicator is based on the cognitive depth reached by the student in a Timer release resource.';
$string['indicator:cognitivedepthdef'] = 'Timer release cognitive';
$string['indicator:cognitivedepthdef_help'] = 'The participant has reached this percentage of the cognitive engagement offered by the Timer release resources during this analysis interval (Levels = No view, View)';
$string['indicator:cognitivedepthdef_link'] = 'Learning_analytics_indicators#Cognitive_depth';
$string['indicator:socialbreadth'] = 'Timer release social';
$string['indicator:socialbreadth_help'] = 'This indicator is based on the social breadth reached by the student in a Timer release resource.';
$string['indicator:socialbreadthdef'] = 'Timer release social';
$string['indicator:socialbreadthdef_help'] = 'The participant has reached this percentage of the social engagement offered by the Timer release resources during this analysis interval (Levels = No participation, Participant alone)';
$string['indicator:socialbreadthdef_link'] = 'Learning_analytics_indicators#Social_breadth';
$string['modulename'] = 'Timer release';
$string['modulename_help'] = 'The timerrelease module enables a teacher to display a number of related files inside a single timerrelease, reducing scrolling on the course page. A zipped timerrelease may be uploaded and unzipped for display, or an empty timerrelease created and files uploaded into it.

A timerrelease may be used

* For a series of files on one topic, for example a set of past examination papers in pdf format or a collection of image files for use in student projects
* To provide a shared uploading space for teachers on the course page (keeping the timerrelease hidden so that only teachers can see it)';
$string['modulename_link'] = 'mod/timerrelease/view';
$string['modulenameplural'] = 'Timer releases';
$string['name'] = 'Name';
$string['newtimerreleasecontent'] = 'New timerrelease content';
$string['page-mod-timerrelease-x'] = 'Any timerrelease module page';
$string['page-mod-timerrelease-view'] = 'Timer release module main page';
$string['privacy:metadata'] = 'The Timer release resource plugin does not store any personal data.';
$string['pluginadministration'] = 'Timer release administration';
$string['pluginname'] = 'Timer release';
$string['display'] = 'Display timerrelease contents';
$string['display_help'] = 'If you choose to display the timerrelease contents on a course page, there will be no link to a separate page. The description will be displayed only if \'Display description on course page\' is ticked. Note that participants view actions cannot be logged in this case.';
$string['displaypage'] = 'On a separate page';
$string['displayinline'] = 'Inline on a course page';
$string['noautocompletioninline'] = 'Automatic completion on viewing of activity can not be selected together with "Display inline" option';
$string['search:activity'] = 'Timer release';
$string['showdownloadtimerrelease'] = 'Show download timerrelease button';
$string['showdownloadtimerrelease_help'] = 'If set to \'yes\', a button will be displayed allowing the contents of the timerrelease to be downloaded as a zip file.';
$string['showexpanded'] = 'Show subtimerreleases expanded';
$string['showexpanded_help'] = 'If set to \'yes\', subtimerreleases are shown expanded by default; otherwise they are shown collapsed.';
$string['maxsizetodownload'] = 'Maximum timerrelease download size (MB)';
$string['maxsizetodownload_help'] = 'The maximum size of timerrelease that can be downloaded as a zip file. If set to zero, the timerrelease size is unlimited.';
