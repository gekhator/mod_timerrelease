GREGS DEV NOTES:

1. Renderable starts on line 127 of renderer.php, most of the code here is modified from blockbase, but its not working so far

2. There is a mix of both output from the renderer here and testing to see rendering from core-renderer via the render from template on line 136 (which is actually from mod_quiz renderer.php). 
This is to explain what i meant about calling theme renderer class core renderers.

3. View.php - Line 75, 76 and 82 are not working because the render method cannot yet render widget which are still not coded to suit ie line 63 of renderer.php

4. Note: The code most likely littered with inactive functions from especially quiz as i try different renderables of the timer




This file is part of Moodle - http://moodle.org/

Moodle is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Moodle is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

copyright 2009 Petr Skoda (http://skodak.org)
license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later


Folder module
=============

Folder module is a successor to original 'directory' type plugin of Resource module.

This module is intended for distribution of large number of documents.

TODO:
 * reimplement portfolio support (MDL-20070)
 * new backup/restore and old restore transformation (MDL-20072)

